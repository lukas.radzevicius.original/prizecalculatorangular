import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SidebarNavComponent } from './components/sidebar-nav/sidebar-nav.component';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome'; 
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { far } from '@fortawesome/free-regular-svg-icons';
import { fab } from '@fortawesome/free-brands-svg-icons';

import { CalculatorComponent } from './components/calculator/calculator.component';
import { SettingsComponent } from './components/settings/settings.component';

import { HttpClientModule } from '@angular/common/http';

import { OrdinalPipe } from './components/pipes/ordinalPipe';
import { SettingsService } from './services/settings.service';

library.add(fas, far, fab);

@NgModule({
  declarations: [
    AppComponent,
    SidebarNavComponent,
    CalculatorComponent,
    SettingsComponent,
    OrdinalPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FontAwesomeModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(private settingsService: SettingsService){
    this.settingsService.localInit();
  }
 }
