import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CalculatorComponent } from './components/calculator/calculator.component';
import { SettingsComponent } from './components/settings/settings.component';

const routes: Routes = [
  { path:'calculator', component:CalculatorComponent },
  { path:'settings', component:SettingsComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
