export interface PrizeCalculationObject { 
    totalMoney: number,
    rake: number,
    prizesPaid: number,
}