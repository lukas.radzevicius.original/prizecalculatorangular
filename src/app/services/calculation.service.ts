import { Injectable } from '@angular/core';
import { PrizeCalculationObject } from '../dtos/PrizeCalculationObject';
import { SettingsService } from './settings.service';

@Injectable({
  providedIn: 'root'
})
export class CalculationService {

  constructor(private settingsService: SettingsService) { }

  calculate(data: PrizeCalculationObject) {
    const settings = this.settingsService.getLocalSettings();
    const totalRake = data.totalMoney * data.rake;
    let totalMoneyPaid = 0;

    let placesResult = [];

    for(let setting of settings) {
      let prizePaid = (data.totalMoney - (data.totalMoney * data.rake)) * setting.prize;
      placesResult.push(prizePaid);
      totalMoneyPaid += prizePaid;
    }

    return {
       placesPrizes: placesResult,
       rakeCollected: totalRake,
       totalPaid: totalMoneyPaid
    }
  }

}
