import { Injectable } from '@angular/core';
import { Observable, ObservedValueOf, catchError, throwError } from 'rxjs';

import { HttpClient, HttpErrorResponse } from '@angular/common/http' 
import { SettingsDto } from '../dtos/settingsDto';
import { ValidatorService } from './validator.service';

@Injectable({
  providedIn: 'root'
})
export class SettingsService {
  settings = 'settings';

  private _url: string = '/assets/settings.json'

  constructor(private http: HttpClient, private validatorService: ValidatorService) { }

  getDefaultSettings(): Observable<SettingsDto[]>{
    return this.http.get<SettingsDto[]>(this._url);
  }

  getLocalSettings() {
    const localSettings = localStorage.getItem(this.settings)
    
    if(localSettings){
      return JSON.parse(localSettings) as SettingsDto[];
    }
    
    throw Error('local settings was empty!?');
  }

  saveSettings(settingsDto: SettingsDto[]){
    const areSettingsValid = this.validatorService.validateSettings(settingsDto, () => {
      localStorage.setItem(this.settings, JSON.stringify(settingsDto))
    },() => {
      throw new Error('Settings are invalid.');
    });
  }

  localInit(){
    if(!localStorage.getItem(this.settings)){
      this.getDefaultSettings().subscribe(data => localStorage.setItem(this.settings, JSON.stringify(data)));
    }
  }
}
