import { Injectable } from '@angular/core';
import { SettingsDto } from '../dtos/settingsDto';

@Injectable({
  providedIn: 'root'
})
export class ValidatorService {

  constructor() { }

  validateSettings(settingsDto: SettingsDto[], validSettings: Function, handleInvalidSettings: Function) {
    let sum = 0;
    
    for (let setting of settingsDto) {
      sum += setting.prize; 

      if(sum > 1){
        break;
      }
    }
    
    if(sum < 1 || sum > 1){
      handleInvalidSettings();
    }
    
    validSettings();
  }
}
