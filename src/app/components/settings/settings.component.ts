import { Component } from '@angular/core';
import { SettingsDto } from 'src/app/dtos/settingsDto';
import { SettingsService } from 'src/app/services/settings.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent {

  editMode: boolean = false;
  errorMsg: any;

  public settings: SettingsDto[] = [];

  constructor(private _settingsService: SettingsService) {}

  ngOnInit(){
    this.loadForm();
  }

  toggleEdit(){
    if(!this.editMode){
      this.enableInputs();
      return;
    }

    this.disableInputs();
  }

  save(){
    const elements: HTMLCollectionOf<HTMLInputElement> = document.getElementsByTagName('input');
    let settingsDto: SettingsDto[] = [];

    for(let i = 0; i < elements.length; i++){
      settingsDto.push({
        place: i + 1,
        prize: +(elements[i].value) / 100
      })
    }

    this._settingsService.saveSettings(settingsDto);
    
    if(this.editMode){
      this.disableInputs();
    }

    this.loadForm();
  }

  private loadForm(){
    this.settings = this._settingsService.getLocalSettings();
    console.log(this.settings)
  }

  private enableInputs(){
    const elements: HTMLCollectionOf<HTMLInputElement> = document.getElementsByTagName('input');
 
    for(let i = 0; i < elements.length; i++){
      elements[i].disabled = false;
    }

    this.editMode = true;
  }

  private disableInputs(){
    const elements: HTMLCollectionOf<HTMLInputElement> = document.getElementsByTagName('input');
 
    for(let i = 0; i < elements.length; i++){
      elements[i].disabled = true;
    }

    this.editMode = false;

  }
}
