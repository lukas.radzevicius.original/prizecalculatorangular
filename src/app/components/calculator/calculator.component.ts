import { Component } from '@angular/core';
import { PrizeCalculationObject } from 'src/app/dtos/PrizeCalculationObject';
import { CalculationService } from 'src/app/services/calculation.service';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.scss']
})
export class CalculatorComponent {

  constructor(private calculationService: CalculationService) {

  }

  calculate() {
      const inputElements = document.getElementsByTagName('input');
      let calculateObject: PrizeCalculationObject = { } as PrizeCalculationObject;

      for(let i = 0; i < inputElements.length; i++) {
        const inputEl = inputElements[i];

        switch(i){
          case 0:
            calculateObject.totalMoney = +(inputEl.value);
            break;
          case 1:
            calculateObject.rake = +(inputEl.value) / 100;
            break;
        }
      }

      console.log(this.calculationService.calculate(calculateObject));
  }
}
